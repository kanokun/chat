const mariadb = require('mariadb/callback');

function connect() {
    const conn = mariadb.createConnection({
        host: 'localhost',
        port: '3306',
        user: 'kanokun',
        password: 'kano',
        database: 'chat'
    });

    return conn;
}

module.exports = { connect }
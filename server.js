const express = require('express');
const session = require('express-session');
const sharedSession = require('socket.io-express-session');
const app = express();
const accounts = require('./router/accounts');
const chats = require('./router/chats');
const games = require('./router/games');
const db = require('./db');

const session_middleware = session({
    secret: 'kan-session-key',
    resave: false,
    saveUninitialized: false,
    cookie: {
        maxAge: 60 * 60 * 1000
    }
});

app.set('views', __dirname + '/public/views');
app.set('view engine', 'ejs');

app.use(
    express.json(),
    express.urlencoded({ extended: false }),
    express.static(__dirname + '/public'),
    session_middleware
);

app.get('/', (req, res)=>{
    res.redirect("/accounts/login");
});
app.use('/accounts', accounts);
app.use('/chats', chats);
app.use('/games', games);

const http = require('http').Server(app);
const io = require('socket.io')(http);
io.use(sharedSession(session_middleware, {
    autoSave:true
}));

const port = 3000;
http.listen(port);
console.log("listening on *:" + port);

// 데이터 받고 보내기
let user_list = [];
let room_list = [];
let game_list = [];

/*
game = {
    room_id : '',
    user_list : [],

    room_alive,
    room_dead,
    room_wolf
}
*/

io.on('connection', socket=>{
    let user = null;
    let session = socket.handshake.session;
    console.log(`${session.login_id}[${socket.id}]`, 'Connected');

    // 로그인 되어있지 않으면 처리
    if(!session.login_id) {
        console.log(`${session.login_id}[${socket.id}]`, 'Disconnected');
        socket.emit('redirect', '/accounts/login');
        socket.disconnect();
        return;
    }

    // 다중 로그인 막기
    let already_logged_in = false;
    user_list.forEach(u=>{
        if(session.login_id == u.login_id)
            already_logged_in = true;
    });

    if(already_logged_in) {
        console.log(`${session.login_id}[${socket.id}]`, 'Disconnected');
        socket.emit('failed', '현재 다른 창에서 활성화 되어 있습니다.');
        socket.disconnect();
        return;
    }

    // room list 전체 전달
    let res_room_list = room_list.map(({pwd, ...rest}) => rest);
    io.sockets.emit('lobby-room-list', res_room_list);

    // lobby user list 전체 전달
    user = {login_id : session.login_id, socket_id : socket.id, location : "lobby", location_id : '' };
    user_list.push(user);
    let _user_list = user_list.map(({pwd, ...rest}) => rest);
    io.sockets.emit('lobby-user-list', _user_list);

    // DB Account 에 socket_id 추가 (요거 필요한가?)
    const conn = db.connect();
    conn.query(`update accounts set socket_id="${socket.id}" where login_id="${session.login_id}" limit 1`, (err, _res)=>{
        if(err)
            console.log(err);

        conn.end();
    });

    // disconnect
    socket.on('disconnect',()=>{

		//delete objects[socket.id];

        // DB Account 에 socket_id 제거
        const conn = db.connect();
        conn.query(`update accounts set socket_id=NULL where login_id="${session.login_id}" limit 1`, (err, _res)=>{
            if(err)
                console.log(err);

            conn.end();
        });

        /*
        user_list.forEach(e=>{
            if(e.socket_id == socket.id)
                console.log(e);
        });
        */


        // Room 의 user list 에서 제거 및 broadcast
        roomLeave();

        // User 리스트 에서 제거 및 broadcast
        user_list = user_list.filter(v => v.socket_id !== socket.id);
        let _user_list = user_list.map(({pwd, ...rest}) => rest);
        io.sockets.emit('lobby-user-list', _user_list);

        // Game 의 user list 에서 socket_id 삭제
        

		console.log(`${session.login_id}[${socket.id}]`, 'Disconnected');
	});

    ///////////////////////////////////////////////////////////////////////////
    // Lobby, Room
    ///////////////////////////////////////////////////////////////////////////
    socket.on('chat-lobby', data=>{
        let _html = `<div>${session.login_id} : ${data}</div>`;

        io.sockets.emit('chat-lobby', _html);
    });

    socket.on('add-room', data=>{
        // room 정보 입력
        let room = {};
        room.id = Date.now();
        room.name = data.name;
        room.pwd = data.pwd;
        room.public = true;
        if(room.pwd != '')
            room.public = false;

        room.user_list = [];
        room.playing = false;

        // user 정보 갱신 및 room 에 user 정보 추가

        if(user.location == "lobby") {
            user.location = "room";
            user.location_id = room.id;
            let _user = { ...user };
            _user.ready = true;
            room.user_list.push(_user);
            room.master = user;
            room.playing = false;
        }

        if(room.user_list.length == 0) {
            socket.emit('failed', '방 생성에 실패하였습니다.');
            return;
        }

        room_list.push(room);

        // room 리스트 전달 (전체)
        let res_room_list = room_list.map(({pwd, ...rest}) => rest);
        io.sockets.emit('lobby-room-list', res_room_list);
        
        let res_room = { ...room };
        delete res_room.pwd;
        socket.emit('join-room', res_room);
        socket.join(room.id);

        io.to(room.id).emit("chat-room", `<div>[${user.login_id}]님이 방에 입장하셨습니다.</div>`);
        io.to(room.id).emit("room-user-list", res_room);

        let _user_list = user_list.map(({pwd, ...rest}) => rest);
        io.sockets.emit('lobby-user-list', _user_list);
    });

    socket.on('join-room', data=>{
        let room = null;
        room_list.forEach(r=>{
            if(r.id == data.id)
                room = r;
        });

        if(!room) {
            socket.emit('failed', "방 입장에 실패하였습니다.");
            return;
        }

        if(!room.public && room.pwd != data.pwd) {
            socket.emit('failed', "방 비밀번호가 일치하지 않습니다.");
            return;
        }

        if(user.location == "lobby") {
            user.location = "room";
            user.location_id = room.id;
            let _user = { ...user };
            _user.ready = false;
            room.user_list.push(_user);
        }

        if(!user) {
            socket.emit('failed', "방 입장에 실패하였습니다.");
            return;
        }

        // room 에 join
        socket.join(room.id);

        // room 정보 pwd 제거 후 전달
        let res_room = { ...room };
        delete res_room.pwd;

        // 추가된 room 에 join
        socket.emit('join-room', res_room);
        io.to(room.id).emit("chat-room", `<div>[${user.login_id}]님이 방에 입장하셨습니다.</div>`);
        io.to(room.id).emit("room-user-list", res_room);

        // lobby user 리스트 전달 (전체, 위치값 변경 : room -> lobby)
        let _user_list = user_list.map(({pwd, ...rest}) => rest);
        io.sockets.emit('lobby-user-list', _user_list);
    });

    socket.on('leave-room', data=>{
        roomLeave();

        socket.emit('leave-room', {});

        let res_user_list = user_list.map(({pwd, ...rest}) => rest);
        io.sockets.emit('lobby-user-list', res_user_list);
    });

    socket.on('chat-room', data=>{
        if(user.location != "room") {
            socket.emit('failed', "잘못된 접근 입니다.");
            return;
        }
        
        io.to(user.location_id).emit("chat-room", `<div>${session.login_id} : ${data}</div>`);
    });

    socket.on(`room-ready`, data=>{
        let room = null;
        room_list.forEach(r=>{
            if(user.location_id == r.id)
                room = r;
        });

        if(!room) {
            socket.emit('failed', "잘못된 접근 입니다.");
            return;
        }

        room.user_list.forEach(u=>{
            if(u.login_id == user.login_id)
                u.ready = !u.ready;
        });
        io.to(room.id).emit("room-user-list", room);
    });

    socket.on('game-start', data=>{
        let room = null;
        room_list.forEach(r=>{
            if(r.id == user.location_id)
                room = r;
        });

        if(!room) {
            socket.emit('failed', "잘못된 접근 입니다.");
            return;
        }

        let all_ready = true;
        room.user_list.forEach(u=>{
            if(room.master.login_id == u.login_id)
                return;

            if(!u.ready)
                all_ready = false;
        });

        /* 나중에 주석 지워야됨
        if(room.user_list.length < 11)
            all_ready = false;
        */

        if(!all_ready) {
            socket.emit('failed', '참가자가 부족하거나, 모두 Ready 상태가 아닙니다.');
            return;
        }

        let game = {};
        game.id = `g-${room.id}`;
        game.user_list = [];
        room.user_list.forEach((u, i) => {
            u.ready = false;
            u.location = 'game';
            u.location_id = game.id;

            let _user = { ...u }
            _user.number = i+1;
            game.user_list.push(_user);
        });
        room.playing = true;
        game_list.push(game);

        // User 위치 수정
        user_list.forEach(u=>{
            u.location = "game";
            u.location_id = game.id;
        });

        // lobby user 리스트 전달 (전체, 위치값 변경 : room -> game)
        let _user_list = user_list.map(({pwd, ...rest}) => rest);
        io.sockets.emit('lobby-user-list', _user_list);

        //io.to(room.id).emit('game-start', {});
        io.to(room.id).emit('redirect', `/games/${game.id}`);
    });

    function roomLeave() {
        let room = null;
        
        if(user.location == "room") {
            let del_room_id = null;
            room_list.forEach(r=>{
                if(r.id == user.location_id) {
                    room = r;

                    user.location = "lobby";
                    user.location_id = "";

                    let is_master = false;
                    let del_room_user_id = null;

                    r.user_list.forEach(u=>{                        
                        if(u.socket_id == socket.id) {
                            del_room_user_id = socket.id;
                            if(r.master.socket_id == socket.id)
                                is_master = true;
                        }
                    });

                    if(del_room_user_id)
                        r.user_list = r.user_list.filter(u => u.socket_id !== del_room_user_id);

                    if(r.user_list.length == 0) {
                        del_room_id = r.id;
                    }else if(is_master){
                        r.master = r.user_list[0];
                    }
                }
            });

            socket.leave(room.id);

            if(del_room_id) {
                room_list = room_list.filter(r => r.id !== del_room_id);
            } else {
                io.to(room.id).emit("chat-room", `<div>[${user.login_id}]님이 방에서 나가셨습니다.</div>`);
                io.to(room.id).emit("room-user-list", room);
            }

            let res_room_list = room_list.map(({pwd, ...rest}) => rest);
            io.sockets.emit('lobby-room-list', res_room_list);
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    // Game
    ///////////////////////////////////////////////////////////////////////////

});
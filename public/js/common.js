// fetch
function Fetch(url, data, returnFunc = null, method = "GET", withoutJson = false) {
    var _mask = document.querySelector('.mask');
    if (_mask)
        _mask.classList.add('show');

    if(!data)
        data = {};

    if (withoutJson) {

        fetch(url, {
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                //RequestVerificationToken: document.getElementById("RequestVerificationToken").value
            },
            method: method,
            body: data
        })
        .then(res => res.text())
        .then(res => {
            if (returnFunc)
                returnFunc(res);
        })
        .finally(() => {
            if (_mask)
                _mask.classList.remove('show');
        });
    } else {
        fetch(url, {
            headers: {
                'Accept': 'application/json; charset=UTF-8',
                'Content-Type': 'application/json; charset=UTF-8'
                //RequestVerificationToken: document.getElementById("RequestVerificationToken").value
            },
            method: method,
            body: JSON.stringify(data)
        })
        .then(res => res.text())
        .then(res => {
            if (returnFunc)
                returnFunc(res);
        })
        .finally(() => {
            if (_mask)
                _mask.classList.remove('show');
        });
    }
}
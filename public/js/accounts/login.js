document.addEventListener('click', e=>{
    if(e.target.classList.contains('btn-login')) {
        var _id = document.querySelector('#id').value.trim();
        var _pwd = document.querySelector('#pwd').value;

        if(_id == '') {
            alert("ID 를 적어주세요.");
            return;
        }
        if(_pwd == '') {
            alert("Password 를 적어주세요.");
            return;
        }

        let _data = {};
        _data["login_id"] = _id;
        _data["password"] = _pwd;

        Fetch("/accounts/login", _data, successLogin, "POST");
    }

    if(e.target.classList.contains('btn-regist')) {
        location.href = "/accounts/regist";
    }
});

function successLogin(result) {
    if(result != 'succeed.') {
        alert("로그인에 실패하였습니다.");
        return;
    }

    location.href = "/chats/lobby";
}

document.addEventListener('click', e=>{
    if(e.target.classList.contains('btn-regist')) {
        var _id = document.querySelector('#id').value.trim();
        var _pwd = document.querySelector('#pwd').value;
        var _pwd_confirm = document.querySelector('#pwd-confirm').value;

        if(_id == '') {
            alert("ID 를 적어주세요.");
            return;
        }
        if(_pwd == '') {
            alert("Password 를 적어주세요.");
            return;
        }
        if(_pwd_confirm == '') {
            alert("Password Confirm 를 적어주세요.");
            return;
        }
        if(_pwd != _pwd_confirm) {
            alert("Password 와 Password Confirm 값이 일치하지 않습니다.");
            return;
        }

        let _data = {};
        _data["login_id"] = _id;
        _data["password"] = _pwd;

        Fetch("/accounts/regist", _data, successRegist, "POST");
    }

    if(e.target.classList.contains('btn-cancel')) {
        location.href = "/accounts/login";
    }
});

function successRegist(result) {
    if(result == 'failed.') {
        alert("계정 생성에 실패하였습니다.");
    }else{
        alert("계정 생성이 완료되었습니다.");
        location.href = "/accounts/login";
    }
}
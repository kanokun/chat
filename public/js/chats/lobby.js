const socket = io();
const chat_log = document.querySelector('.chat-log');
const user_list = document.querySelector('.user-list');
const room_list = document.querySelector('.room-list');

const room_users = document.querySelectorAll('.room-user');
const room_chat_log = document.querySelector('.room-chat-log');

document.addEventListener('click', e=>{
    if(e.target.classList.contains('btn-logout')) {
        Fetch("/accounts/logout", null, successLogout, "POST");
    }

    if(e.target.classList.contains('btn-chat-send')) {
        sendLobbyChat();
    }

    if(e.target.classList.contains('btn-add-room-popup')) {
        document.querySelector('.popup-add-room').classList.remove('hide');
    }

    if(e.target.classList.contains('btn-popup-close')) {
        document.querySelector('.popup-add-room').classList.add('hide');
    }

    if(e.target.classList.contains('btn-add-room')) {
        let room_name = document.querySelector('#popup-room-name').value.trim();
        let room_pwd = document.querySelector('#popup-room-pwd').value;
        
        socket.emit(`add-room`, { name : room_name, pwd : room_pwd });

        document.querySelector('#popup-room-name').value = '';
        document.querySelector('#popup-room-pwd').value = '';
    }

    if(e.target.classList.contains('btn-room-join')) {
        console.log(e.target.dataset.id);

        let req = {};
        if(e.target.dataset.private == "true")
            req.pwd = prompt("비밀번호를 입력해주세요.");
        req.id = e.target.dataset.id;

        socket.emit('join-room', req);
    }

    if(e.target.classList.contains('btn-room-chat-send')) {
        sendRoomChat();
    }

    if(e.target.classList.contains('btn-room-start')) {
        console.log(room_users);

        let all_ready = true;
        room_users.forEach(e=>{
            let _ready = e.querySelector('.room-user-ready').innerText;
            if(!(_ready == "Master" || _ready == "Ready")) {
                all_ready = false;
            }
        });

        // 임시
        all_ready = true;
        
        if(!all_ready) {
            alert("참가자가 부족하거나, 모두 Ready 상태가 아닙니다.");
            return;
        }

        socket.emit('game-start', {});

    }else if(e.target.classList.contains('btn-room-ready')) {
        
        socket.emit('room-ready', {});
    }

    if(e.target.classList.contains('btn-room-leave')) {
        socket.emit('leave-room', {});
    }
});

document.addEventListener('keyup', e=>{
    if(e.target.id == "chat-input" && e.code == "Enter") {
        console.log('keydown');
        sendLobbyChat();
    }

    if(e.target.id == "room-chat-input" && e.code == "Enter") {
        console.log('keydown');
        sendRoomChat();
    }
});

function successLogout(result) {
    location.href = "/accounts/login";
}

function sendLobbyChat() {
    let chat_text = document.querySelector('#chat-input').value;
    if(chat_text == "")
        return;

    document.querySelector('#chat-input').value = '';
    socket.emit(`chat-lobby`, chat_text);
}

function sendRoomChat() {
    let chat_text = document.querySelector('#room-chat-input').value;
    if(chat_text == "")
        return;

    document.querySelector('#room-chat-input').value = '';
    socket.emit(`chat-room`, chat_text);
}

function initRoomUser() {
    room_users.forEach(e=>{
        e.classList.add('empty');
        e.querySelector('.room-user-id').innerHTML = '';
        e.querySelector('.room-user-ready').innerHTML = '';
    });
}

socket.on('chat-lobby', result=>{
    console.log(result);
    chat_log.innerHTML += result;
});

socket.on('lobby-user-list', result=>{
    user_list.innerHTML = ``;

    result.forEach(e=>{
        user_list.innerHTML += `<div>${e.login_id}_${e.location}</div>`;
    });
});

socket.on('lobby-room-list', result=>{

    console.log(result);

    let html = ``;
    result.forEach(e=>{
        html += `
            <div class="room">
                <p class="room-name">
                    <span>${e.name}</span>
                    <b>${e.public?'':'비공개'}</b>
                </p>
                <span class="room-master">${e.master.login_id}</span>
                <span class="room-user-count">${e.user_list.length}/11</span>
                <button class="btn-room-join" data-id=${e.id} data-private=${e.public?false:true}>입장</button>
            </div>`;
    });

    room_list.innerHTML = html;
});

socket.on('join-room', result=>{
    console.log(result);
    //chat_log.innerHTML += result;
    document.querySelector('.popup-add-room').classList.add('hide');
    document.querySelector('.wrap-rooms').classList.add('hide');
    document.querySelector('.wrap-in-room').classList.remove('hide');
});

socket.on('leave-room', result=>{
    document.querySelector('.wrap-in-room').classList.add('hide');
    document.querySelector('.wrap-rooms').classList.remove('hide');

    //room_user_list.innerHTML = '';
    room_chat_log.innerHTML = '';
    document.querySelector('#room-chat-input').value = '';

    // room-user 초기화
    initRoomUser();
});

socket.on('chat-room', result=>{
    console.log(result);
    room_chat_log.innerHTML += result;
});

socket.on('room-user-list', result=>{
    initRoomUser();

    result.user_list.forEach((e, i)=>{
        room_users[i].classList.remove('empty');
        room_users[i].querySelector('.room-user-id').innerHTML = e.login_id;

        if(result.master.login_id == e.login_id) {
            room_users[i].querySelector('.room-user-ready').innerHTML = 'Master';
        }else if(e.ready) {
            room_users[i].querySelector('.room-user-ready').innerHTML = 'Ready';
        }
    });

    if(result.master.login_id == document.querySelector('.login-id').innerText) {
        document.querySelector('.btn-room-ready').classList.add("btn-room-start");
        document.querySelector('.btn-room-ready').innerText = "시작";
    }else{
        document.querySelector('.btn-room-ready').classList.remove("btn-room-start");
        document.querySelector('.btn-room-ready').innerText = "준비";
    }
});

socket.on('game-start', result=>{
    console.log(result);
});

socket.on('failed', result=>{
    console.log(result);
    alert(result);
});

socket.on('redirect', result=>{
    location.href = result;
});
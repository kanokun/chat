const express = require('express');
const router = express.Router();
const path = require('path');
let login_id = "";

function isLoggedIn(req, res, next) {
    if (req.session.login_id) {
        login_id = req.session.login_id;
        next();  // 인증된 경우 다음 미들웨어로 넘어감
    } else {
        //res.status(401).send('Unauthorized');  // 인증되지 않은 경우
        return res.redirect("/accounts/login");
    }
}

router.get('/:gid', isLoggedIn, (req, res) => {
    //req.params.gid

    res.render('games/index', { login_id: login_id });
    //res.sendFile(path.join(__dirname + '/../public/views/chats/lobby'));
});

module.exports = router;
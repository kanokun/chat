const express = require('express');
const router = express.Router();
const path = require('path');
const bcrypt = require('bcrypt');
const db = require('../db');
//const session = require('express-session');

// middleware that is specific to this router
/*
const timeLog = (req, res, next) => {
  console.log('Time: ', Date.now())
  next()
}
router.use(timeLog)
*/

// define the home page route
router.get('/login', (req, res) => {
    //res.sendFile(path.join(__dirname + '/../public/views/accounts/login'));
    res.render('accounts/login', {});
});

router.post('/login', (req, res) => {
    const conn = db.connect();

    const id = req.body.login_id;
    const pwd = req.body.password;
    
    conn.query(`select * from accounts where login_id="${id}" limit 1`, (err, _res)=>{
        if(err) {
            console.log(err);
            res.send('failed.'); 
        }else{
            if(_res.length != 0) {
                bcrypt.compare(pwd, _res[0].password, (err, same) => {
                    if(same) {
                        req.session.login_id = id;
                        res.send('succeed.');
                    }else{
                        console.log(`login_id(${id})'s password is wrong.`);
                        res.send('failed.');
                    }
                });
            }else{
                console.log(`login_id(${id}) is not exist.`);
                res.send('failed.');
            }
        }

        conn.end();
    });
});

router.post('/logout', (req, res) => {
    delete req.session.login_id;

    res.send(`succeed.`);
});

router.get('/regist', (req, res) => {
    //res.sendFile(path.join(__dirname + '/../public/views/accounts/regist'));
    res.render('accounts/regist', {});
});

router.post('/regist', (req, res) => {
    const conn = db.connect();

    const id = req.body.login_id;
    const pwd = bcrypt.hashSync(req.body.password, 6); //비밀번호 암호화

    conn.query(`insert into accounts(login_id, password) values ("${id}", "${pwd}")`, (err, _res)=>{
        
        if(err) {
            console.log(err);
            res.send('failed.'); 
        }else{
            res.send('succeed.');
        }

        conn.end();
    });
});

// 로그아웃용 세션 제거 예제
//delete req.session.cart;  // cart 키만 삭제
/*  여긴 전체 제거
req.session.destroy(err => {
    if (err) {
        return res.status(500).send("Failed to clear session");
    }
    res.send("Session cleared");
});
*/

module.exports = router;